import Game from "./classes/Game";
import "./css/main.css";

window.addEventListener("DOMContentLoaded", () => {
  const game = new Game("gameCanvas");
});
