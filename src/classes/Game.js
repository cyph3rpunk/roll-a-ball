import {
  Engine,
  Scene,
  ArcRotateCamera,
  Vector3,
  Color3,
  HemisphericLight,
  DirectionalLight,
  ShadowGenerator,
  BabylonFileLoaderConfiguration,
  CannonJSPlugin
} from "babylonjs";
import * as foo from "cannon";

import Ground from "./Ground";
import Cube from "./Cube";
import Player from "./Player";

const NUM_OF_CUBES = 10;
const GROUND_SIZE = 20;

class Game {
  constructor(canvasId) {
    const canvas = document.getElementById(canvasId);
    this.engine = new Engine(canvas, true);
    this.scene = new Scene(this.engine);
    this.scene.ambientColor = Color3.White();
    this.camera = new ArcRotateCamera(
      "camera",
      Math.PI / 2,
      Math.PI / 3,
      35,
      Vector3.Zero(),
      this.scene
    );
    this.camera.setTarget(Vector3.Zero());
    this.camera.attachControl(this.engine.getRenderingCanvas(), true);
    this.light = new HemisphericLight(
      "light",
      new Vector3(0, 1, 0),
      this.scene
    );

    this.dirLight = new DirectionalLight(
      "dirLight",
      new Vector3(0, -1, -1),
      this.scene
    );
    this.dirLight.position = new Vector3(0, 20, 0);
    this.shadows = new ShadowGenerator(1024, this.dirLight);
    this.shadows.useBlurExponentialShadowMap = true;
    this.shadows.setTransparencyShadow(true);

    BabylonFileLoaderConfiguration.LoaderInjectedPhysicsEngine = foo;
    this.scene.enablePhysics(new Vector3(0, -9.81, 0), new CannonJSPlugin());

    this.ground = new Ground(GROUND_SIZE, this);
    this.ground.rotation.x = Math.PI / 2;
    this.placeCubes();
    this.player = new Player(1, this);
    this.player.position = new Vector3(0, 1, 0);

    this.scene.registerBeforeRender(() => {
      let idx;
      this.cubes.forEach((cube, i) => {
        if (cube.intersectsMesh(this.player)) {
          cube.dispose();
          idx = i;
        }
      });
      if (idx !== undefined) this.cubes.splice(idx, 1);
      document.getElementById(
        "info"
      ).innerText = `Cubes left: ${this.cubes.length}`;
    });

    this.engine.runRenderLoop(() => {
      this.scene.render();
    });
  }

  placeCubes() {
    this.cubes = [];
    for (let i = 0; i < NUM_OF_CUBES; i++) {
      const cube = new Cube(0.35, this);
      cube.position.y = 0.5;
      cube.rotation.x = Math.PI / 4;
      cube.rotation.z = Math.PI / 4;
      const max = GROUND_SIZE / 2 - 1.5;
      const min = -GROUND_SIZE / 2 + 1.5;
      cube.position.x = Math.random() * (max - min) + min;
      cube.position.z = Math.random() * (max - min) + min;
      this.cubes.push(cube);
    }
  }
}

export default Game;
