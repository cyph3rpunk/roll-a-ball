import { Mesh } from "babylonjs";

class GameObject extends Mesh {
  constructor(name, game) {
    super(name, game.scene);
    this.game = game;
    this.scene = game.scene;
  }
}

export default GameObject;
