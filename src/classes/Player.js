import {
  VertexData,
  StandardMaterial,
  Color3,
  Vector3,
  PhysicsImpostor,
  FresnelParameters
} from "babylonjs";
import GameObject from "./GameObject";

class Player extends GameObject {
  constructor(diameter, game) {
    super("player", game);
    const vertexData = VertexData.CreateSphere({ diameter });
    vertexData.applyToMesh(this);

    const material = new StandardMaterial("player material", game.scene);

    material.diffuseColor = new Color3(0.3, 0, 0.8);
    material.emissiveColor = new Color3(0.3, 0, 0.8);
    material.alpha = 0.6;
    material.specularPower = 16;
    material.specularColor = new Color3(0.7, 0.7, 1);

    // Fresnel
    material.reflectionFresnelParameters = new FresnelParameters();

    material.emissiveFresnelParameters = new FresnelParameters();
    material.emissiveFresnelParameters.bias = 0.6;
    material.emissiveFresnelParameters.power = 4;
    material.emissiveFresnelParameters.leftColor = Color3.White();
    material.emissiveFresnelParameters.rightColor = Color3.Black();

    material.opacityFresnelParameters = new FresnelParameters();
    material.opacityFresnelParameters.leftColor = Color3.White();
    material.opacityFresnelParameters.rightColor = Color3.Black();

    this.material = material;
    game.shadows.getShadowMap().renderList.push(this);

    // physics
    this.physicsImpostor = new PhysicsImpostor(
      this,
      PhysicsImpostor.SphereImpostor,
      {
        mass: 5,
        friction: 0.9,
        restitution: 0.9
      },
      game.scene
    );

    this.direction = Vector3.Zero();
    // user input
    window.addEventListener("keydown", event => {
      switch (event.keyCode) {
        case 87: // W
          this.direction = new Vector3(0, 0, -1);
          break;
        case 65: // A
          this.direction = new Vector3(1, 0, 0);
          break;
        case 83: // S
          this.direction = new Vector3(0, 0, 1);
          break;
        case 68: // D
          this.direction = new Vector3(-1, 0, 0);
          break;
        default:
      }
    });

    window.addEventListener("keyup", event => {
      this.direction = Vector3.Zero();
    });

    this.getScene().registerBeforeRender(() => {
      // move player.
      this.applyImpulse(this.direction, this.position);
    });
  }
}

export default Player;
