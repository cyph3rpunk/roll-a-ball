const path = require("path");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const parts = require("./webpack.parts");

const PATHS = {
  app: path.join(__dirname, "src")
};

const commonConfig = merge([
  {
    plugins: [
      new HtmlWebpackPlugin({
        title: "Roll-a-Ball",
        template: "src/index.html"
      })
    ]
  },
  parts.loadJavaScript({ include: PATHS.app })
]);

const productionConfig = merge([parts.extractCSS({ use: "css-loader" })]);

const developmentConfig = merge([
  parts.devServer({
    // customize host/port here if needed
    host: process.env.HOST,
    port: process.env.PORT
  }),
  parts.loadCSS()
]);

module.exports = mode => {
  if (mode === "production") {
    return merge(commonConfig, productionConfig, { mode });
  }

  return merge(commonConfig, developmentConfig, { mode });
};
